import os
import sys
import math
from pulp import *
from pygraph.classes.digraph import digraph
from pygraph.algorithms.minmax import shortest_path

currIdChildren = 0
currIdBed = 0

class Child:

  def __init__(self):
    global currIdChildren
    self.identifier = currIdChildren
    currIdChildren += 1
    self.shifts = []
    self.friends = []
    self.enemy = []
    self.solution = []
    self.group = 0

class Bed:

  def __init__(self):
    global currIdBed
    self.identifier = currIdBed
    currIdBed += 1
    self.neighbors = []
    self.accessible = []
    self.distance = []

class Model:
  
  def __init__(self):
    self.model = LpProblem("Bed Assignment Problem", sense=LpMinimize)
    self.children = []
    self.beds = []
    self.nbShifts = 7
  
  def createVariables(self):
    # First we create the y variables indicating that a given child
    # uses a given bed at least once.
    # First dimension of y are the children
    self.y = []
    for c in range(len(self.children)):
      self.y.append([])
      for b in range(len(self.beds)):
        self.y[-1].append(LpVariable("y_"+str(c)+"_"+str(b),lowBound=0,upBound=1,cat = LpInteger))
    # Then we create the x variables indicating if a child is in a bed
    # for a given shift
    self.x = []
    for c in range(len(self.children)):
      self.x.append([])
      for b in range(len(self.beds)):
        self.x[-1].append([])
        for s in range(self.nbShifts):
          self.x[-1][-1].append(LpVariable("x_"+str(c)+"_"+str(b)+"_"+str(s),lowBound=0,upBound=1,cat = LpInteger))

  def createConstraintOneBedPerChildWhenThere(self):
    # We create the constraint stating that each child has a bed when needed.
    for c in self.children:
      for s in c.shifts:
        cst = lpSum([self.x[c.identifier][b][s] for b in range(len(self.beds))]) == 1
        cst.name = "HasABedForShift_"+str(c.identifier)+"_"+str(s)
        self.model += cst
 
  def createConstraintNoMoreThanOneChildPerBed(self):
    # We create the constraint stating that two children cannot sleep in
    # the same bed at the same time.
    for s in range(self.nbShifts):
      for b in range(len(self.beds)):
        cst = lpSum([self.x[c][b][s] for c in range(len(self.children))]) <= 1
        cst.name = "noMoreThanOnePerBed_"+str(s)+"_"+str(b)
        self.model += cst
  
  def createConstraintUseOnShiftIfUseInGeneral(self):
    # We create the constraint stating that a x variable can only be equal
    # to one if the associated y variable is also equal to one.
    for c in range(len(self.children)):
      for b in range(len(self.beds)):
        for s in range(self.nbShifts):
          cst = self.x[c][b][s] <= self.y[c][b]
          cst.name = "useOnShiftIfUseInGeneral_"+str(c)+"_"+str(b)+"_"+str(s)
          self.model += cst
 
  def createConstraintNotInBedIfNotThere(self):
    # We create the constraint stating that when a kid is not there, he
    # does not use a bed
    for c in self.children:
      for s in range(self.nbShifts):
        if not s in c.shifts:
          for b in range(len(self.beds)):
            cst = self.x[c.identifier][b][s] == 0
            cst.name = "notInBedIfNotThere_"+str(c.identifier)+"_"+str(s)+"_"+str(b)
            self.model += cst

  def createConstraintFarFromEnemy(self):
    # We create the constraint stating that two enemies should not be
    # in close beds at the same time.
    for c in self.children:
      for cc in c.enemy:
        for s in range(self.nbShifts):
          if s in c.shifts and s in self.children[cc].shifts:
            for b in self.beds:
              cst = self.x[c.identifier][b.identifier][s] + lpSum([self.x[cc][bb][s] for bb in b.neighbors]) <= 1
              cst.name = "farFromEnemy_"+str(c.identifier)+"_"+str(cc)+"_"+str(b.identifier)+"_"+str(s)
              self.model += cst
  def createConstraintCloseToFriends(self):
    # We create the constraint stating that two friends should not be far
    # away
    for c in self.children:
      for cc in c.friends:
        for s in range(self.nbShifts):
          if s in c.shifts and s in self.children[cc].shifts:
            for b in self.beds:
              cst = self.x[c.identifier][b.identifier][s] <= lpSum([self.x[cc][bb][s] for bb in b.neighbors])
              cst.name = "closeToFriends_"+str(c.identifier)+"_"+str(cc)+"_"+str(b.identifier)+"_"+str(s)
              self.model += cst

  def createConstraintGroupsTogether(self):
    # We create the constraints stating that the children
    # that are the responsibility of the same staff member
    # must be assigned beds that are close together.
    self.createGroups()
    for g in self.groups:
      for b in self.beds:
        for bb in self.beds:
          if b.distance[bb.identifier] >= len(g):
            for s in range(self.nbShifts):
              cst = lpSum([self.x[c.identifier][b.identifier][s] for c in g]) + lpSum([self.x[c.identifier][bb.identifier][s] for c in g]) <= 1
              cst.name = "incompatibleBeds_"+str(g[0].identifier)+"_"+str(b.identifier)+"_"+str(bb.identifier)+"_"+str(s)
              self.model += cst

  def createGroups(self):
    # We create the groups of children that are the responsibility of
    # the same staff member.
    idMax = 0
    for c in self.children:
      if c.group > idMax:
        idMax = c.group
    self.groups = [[] for i in range(idMax+1)]
    for c in self.children:
      self.groups[c.group].append(c)

  def createConstraints(self):
    # We create all the constraints of the problem.
    self.createConstraintOneBedPerChildWhenThere()
    self.createConstraintNoMoreThanOneChildPerBed()
    self.createConstraintUseOnShiftIfUseInGeneral()
    self.createConstraintNotInBedIfNotThere()
    self.createConstraintFarFromEnemy()
    self.createConstraintCloseToFriends()
    self.createConstraintGroupsTogether()

  def createObjectiveFunction(self):
    self.model += lpSum([lpSum(self.y[c]) for c in range(len(self.children))])


  def solve(self):
    # We solve the model using GLPK
    status = self.model.solve(GLPK(msg = 1))
    return LpStatus[status]

def dijkstra(bed,bedGraph,beds):
  sp = shortest_path(bedGraph,bed.identifier)
  dists = []
  for b in beds:
    if not sp[1][b.identifier] == None:
      dists.append(sp[1][b.identifier])
    else:
      dists.append(1000)
  return dists

def computeAccessibility(beds,bedGraph):
  # We compute the distance between each pair
  # of beds.
  for b in beds:
    b.distance = dijkstra(b,bedGraph,beds)
    print b.identifier, b.distance

# Arg 1 -> path to the instance file
# Arg 2 -> path to the output file
def solveProblem(pathIn,pathOut):
  f = open(pathIn,'r')
  lines = f.readlines()
  nbChildren = int(lines[0].split(' ')[0])
  nbBeds = int(lines[0].split(' ')[1])
  nbShifts = int(lines[0].split(' ')[2])
  nbStaff = int(lines[0].split(' ')[3])
  children = {}
  beds = {}
  for c in range(nbChildren):
    children[lines[1+c*4][:-1]] = Child()
    for s in lines[2+c*4].split(' '):
      children[lines[1+c*4][:-1]].shifts.append(int(s))
  for c in range(nbChildren):
    if not lines[3+c*4] == "\n":
      for f in lines[3+c*4].split(' '):
        if '\n' in f:
          f = f[:-1]
        children[lines[1+c*4][:-1]].friends.append(children[f].identifier)
    if not lines[4+c*4] == "\n":
      for e in lines[4+c*4].split(' '):
        if '\n' in e:
          e = e[:-1]
        children[lines[1+c*4][:-1]].enemy.append(children[e].identifier)
  for b in range(nbBeds):
    beds[lines[1+nbChildren*4+b].split(' ')[0]] = Bed()
  for b in range(nbBeds):
    for bb in lines[1+nbChildren*4+b].split(' ')[1:]:
      if '\n' in bb:
        bb = bb[:-1]
      beds[lines[1+nbChildren*4+b].split(' ')[0]].neighbors.append(beds[bb].identifier)
  for s in range(nbStaff):
    for ing in lines[1+nbChildren*4+nbBeds+s].split(' ')[1:]:
      if '\n' in ing:
        ing = ing[:-1]
      children[ing].group = s
  bedGraph = digraph()
  bedGraph.add_nodes([b.identifier for b in beds.values()])
  print beds
  for b in beds.values():
    for bb in b.neighbors:
      bedGraph.add_edge((b.identifier,bb),1)
  computeAccessibility(sorted(beds.values(), key=lambda b: b.identifier),bedGraph)
  model = Model()
  for cc in range(len(children.keys())):
    for c in children.keys():
      if children[c].identifier == cc:
        model.children.append(children[c])
  for bb in range(len(beds.keys())):
    for b in beds.keys():
      if beds[b].identifier == bb:
        model.beds.append(beds[b])
  model.nbShifts = nbShifts
  model.createVariables()
  model.createConstraints()
  model.createObjectiveFunction()
  print model.solve()
  for c in children.values():
    cId = c.identifier
    for s in range(nbShifts):
      assigned = ""
      for b in beds.keys():
        bId = beds[b].identifier
        if model.x[cId][bId][s].value() == 1:
          assigned = b
      c.solution.append(assigned)
  fout = open(pathOut,'w')
  fout.write("Children")
  for i in range(nbShifts):
    fout.write(";"+str(i+1))
  fout.write("\n")
  for c in children:
    fout.write(c)
    for b in children[c].solution:
      fout.write(";"+b)
    fout.write("\n")
  fout.close()
  


def main():
  args = sys.argv[1:]
  solveProblem(args[0],args[1])


if __name__ == "__main__":
  main()

